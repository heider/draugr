draugr.os\_utilities.windows\_utilities.task\_scheduler.api
===========================================================

.. automodule:: draugr.os_utilities.windows_utilities.task_scheduler.api

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      new_user_logon_execute_task
      delete_task
      set_task_activity
   
   

   
   
   

   
   
   



