draugr.os\_utilities.windows\_utilities.task\_scheduler.enums.task\_action\_type.TaskActionTypeEnum
===================================================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums.task_action_type

.. autoclass:: TaskActionTypeEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TaskActionTypeEnum.TASK_ACTION_EXEC
      ~TaskActionTypeEnum.TASK_ACTION_COM_HANDLER
      ~TaskActionTypeEnum.TASK_ACTION_SEND_MAIL
      ~TaskActionTypeEnum.TASK_ACTION_SHOW_MESSAGE
   
   