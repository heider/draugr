draugr.os\_utilities.windows\_utilities.task\_scheduler.enums.task\_trigger.TaskTriggerEnum
===========================================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums.task_trigger

.. autoclass:: TaskTriggerEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TaskTriggerEnum.TASK_TRIGGER_EVENT
      ~TaskTriggerEnum.TASK_TRIGGER_TIME
      ~TaskTriggerEnum.TASK_TRIGGER_DAILY
      ~TaskTriggerEnum.TASK_TRIGGER_WEEKLY
      ~TaskTriggerEnum.TASK_TRIGGER_MONTHLY
      ~TaskTriggerEnum.TASK_TRIGGER_MONTHLYDOW
      ~TaskTriggerEnum.TASK_TRIGGER_IDLE
      ~TaskTriggerEnum.TASK_TRIGGER_REGISTRATION
      ~TaskTriggerEnum.TASK_TRIGGER_BOOT
      ~TaskTriggerEnum.TASK_TRIGGER_LOGON
      ~TaskTriggerEnum.TASK_TRIGGER_SESSION_STATE_CHANGE
   
   